## Consideraciones del proyecto

Se debe configurar adecuadamente las siguientes variables en el archivo .env:

* Usuarios de base de datos
* Log de laravel en dialy
* Credenciales de correo electrónico

Cada que se realice un cambio en el proceso de envío de email, se debe reiniciar 
la cola de procesos, dado que el código en memoria no corresponde al editado

La cola de procesamiento verificar que no haya emails con procesos fallidos,
cuando finaliza el envío del email el estado cambia a enviado (true).
