<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Mail\EmailEnviado;
use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcesarEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $remitente;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Email $email, $remitente)
    {
        $this->email = $email;
        $this->remitente = $remitente;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->email->destinatario)
            ->send(new EmailEnviado($this->email, $this->remitente));

        $this->email->estado = true;
        $this->email->save();

        echo "Email enviado\n";
    }
}
