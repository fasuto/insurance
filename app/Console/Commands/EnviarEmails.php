<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class EnviarEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:enviar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permite el envío de emails masivo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "Intentando reenviar emails...\n";
        $this->call('queue:retry',[
            '--queue' => 'envioEmails'
        ]);
        echo "Enviando emails...\n";
        $this->call('queue:work', [
            '--queue' => 'envioEmails'
        ]);
        echo "Emails enviados...\n";
    }
}
