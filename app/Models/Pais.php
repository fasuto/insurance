<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;

    protected $table = 'paises';

    /**
     * Atributos de asignación masiva
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];

    public function estados(){
        return $this->hasMany(Estado::class);
    }
}
