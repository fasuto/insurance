<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'cedula',
        'celular',
        'ciudad_id',
        'email',
        'password',
        'admin',
        'fec_nac'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*
     * Permite calcular la edad del usuario
     * */
    public function getEdadAttribute(){
        $fec_nac = Carbon::createFromDate($this->fec_nac);
        $now = Carbon::now();
        return $fec_nac->diffInYears($now).' años';
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }

    public function emails(){
        return $this->hasMany(Email::class);
    }

    /*
     * Verifica si el usuario es un administrador
     * */
    public function isAdmin(){
        return $this->admin;
    }
}
