<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use HasFactory;

    protected $table = 'estados';

    /**
     * Atributos de asignación masiva
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'pais_id'
    ];

    public function ciudades(){
        return $this->hasMany(Ciudad::class);
    }

    public function pais(){
        return $this->belongsTo(Pais::class);
    }
}
