<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    use HasFactory;

    protected $table = 'ciudades';

    /**
     * Atributos de asignación masiva
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'estado_id'
    ];

    public function users(){
        return $this->hasMany(User::class);
    }

    public function estado(){
        return $this->belongsTo(Estado::class);
    }
}
