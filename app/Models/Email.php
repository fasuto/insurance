<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    protected $table = 'emails';

    /**
     * Atributos de asignación masiva
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'asunto',
        'destinatario',
        'mensaje',
        'user_id'
    ];

    public function usuario(){
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
