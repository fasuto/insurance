<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Ciudad;
use App\Models\Estado;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{
    public function estados(Request $request){
        $request->validate([
            'pais' => 'required|integer|min:1'
        ]);

        $estados = Estado::where('pais_id',$request->pais)->get(['id','nombre']);

        return response()->json($estados);
    }

    public function ciudades(Request $request){
        $request->validate([
            'estado' => 'required|integer|min:1'
        ]);

        $ciudad = Ciudad::where('estado_id',$request->estado)->get(['id','nombre']);

        return response()->json($ciudad);
    }
}
