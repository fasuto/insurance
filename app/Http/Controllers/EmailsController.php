<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\ProcesarEmails;
use App\Models\Email;
use Illuminate\Http\Request;
use DB;

class EmailsController extends Controller
{
    public function index(Request $request){

        $request->validate([
            'porPagina' => 'nullable|integer|min:1',
            'filtro' => 'nullable|string|max:12',
        ]);

        $porPagina = (empty($request->porPagina)) ? 5 : $request->porPagina;

        $query = Email::where(function($q) use ($request){
            $q->where('asunto', 'like', '%'.$request->filtro.'%')
                ->orWhere('destinatario', 'like', '%'.$request->filtro.'%');
        });

        /*
         * Controla que solo el administrador vea todos los email
         */
        if(!auth()->user()->isAdmin()){
            $query->where('user_id',auth()->user()->id);
        }

        $emails = $query->orderBy('id','desc')->paginate($porPagina);


        return view('emails.lista',[
            'emails'=>$emails,
            'filtro'=>$request->filtro,
            'porPagina'=>$porPagina,
        ]);
    }

    public function crear(){
        return view('emails.crear');
    }

    public function almacenar(Request $request){
        $request->validate([
            'asunto' => 'required|string|max:200',
            'destinatario' => 'required|string|email',
            'mensaje' => 'required|string',
        ]);

        DB::beginTransaction();
        try{
            $email = Email::create([
                'asunto' => $request->asunto,
                'destinatario' => $request->destinatario,
                'mensaje' => $request->mensaje,
                'estado' => false,
                'user_id' => auth()->user()->id
            ]);
            DB::commit();
            ProcesarEmails::dispatch($email, auth()->user()->email)->onQueue('envioEmails');
            return redirect('/emails');
        }catch (\Exception $ex){
            DB::rollBack();
            report($ex);
            return redirect()->back()->with(['errorInterno'=>'Ha ocurrido un error'])->withInput($request->all());
        }
    }

    public function endpoint(Request $request){

        $request->validate([
            'porPagina' => 'nullable|integer|min:1|max:20',
            'filtro' => 'nullable|string|max:100',
        ]);

        $porPagina = (empty($request->porPagina)) ? 5 : $request->porPagina;

        $filtro = htmlentities($request->filtro ?? '', ENT_QUOTES, 'UTF-8', false);

        $emails = Email::with(['usuario:id,email'])
                ->whereHas('usuario', function($q) use ($filtro){
                    return $q->where('email','like','%'.$filtro.'%')->select(['id','email']);
                })
                ->orWhere('asunto', 'like', '%'.$filtro.'%')
                ->orWhere('destinatario', 'like', '%'.$filtro.'%')
                ->orderBy('created_at','desc')->paginate($porPagina);

        return view('endpoints.emails',[
            'emails'=>$emails,
            'filtro'=>$request->filtro,
            'porPagina'=>$porPagina,
        ]);
    }
}
