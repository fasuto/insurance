<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Pais;
use App\Rules\MayorEdadRule;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    public function index(Request $request){

        $request->validate([
            'porPagina' => 'nullable|integer|min:1',
            'filtro' => 'nullable|string|max:12',
        ]);

        $porPagina = (empty($request->porPagina)) ? 5 : $request->porPagina;

        $usuarios = User::with('ciudad:id,nombre')
            ->where('nombre', 'like', '%'.$request->filtro.'%')
            ->orWhere('cedula', 'like', '%'.$request->filtro.'%')
            ->orWhere('celular', 'like', '%'.$request->filtro.'%')
            ->orWhere('email', 'like', '%'.$request->filtro.'%')
            ->orderBy('id','desc')->paginate($porPagina);

        return view('usuarios.lista',[
            'usuarios'=>$usuarios,
            'filtro'=>$request->filtro,
            'porPagina'=>$porPagina,
        ]);
    }

    public function create(){
        $paises = Pais::get(['id','nombre']);
        return view('usuarios.crear',['paises'=>$paises]);
    }

    public function store(Request $request){
        $request->validate([
            'nombre' => 'required|string|max:100',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).*$/',
            'email' => 'required|string|email|unique:users,email',
            'celular' => 'nullable|digits:10',
            'cedula' => 'required|digits_between:1,11',
            'fechaNacimiento' => ['required','date',new MayorEdadRule()],
            'ciudad' => 'required|integer|exists:ciudades,id',
        ],[
            'password.regex' => 'Su contraseña debe tener un número, una letra mayúscula, un carácter especial'
        ]);

        DB::beginTransaction();
        try{
            User::create([
                'nombre' => $request->nombre,
                'password' => bcrypt($request->password),
                'email' => $request->email,
                'celular' => $request->celular,
                'cedula' => $request->cedula,
                'fec_nac' => $request->fechaNacimiento,
                'ciudad_id' => $request->ciudad,
                'admin' => false
            ]);
            DB::commit();
            return redirect('/usuarios');
        }catch (\Exception $ex){
            DB::rollBack();
            report($ex);
            return redirect()->back()->with(['errorInterno'=>'Ha ocurrido un error'])->withInput($request->all());
        }
    }

    public function editar($usuario){
        $usuario = User::with(['ciudad:id,nombre,estado_id','ciudad.estado:id,nombre,pais_id'])->findOrFail($usuario);
        $paises = Pais::get(['id','nombre']);
        return view('usuarios.editar',[
            'usuario'=>$usuario,
            'paises'=>$paises,
        ]);
    }

    public function actualizar(Request $request, User $usuario){
        $request->validate([
            'nombre' => 'required|string|max:100',
            'password' => 'nullable|string|min:8|confirmed|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).*$/',
            'celular' => 'nullable|digits:10',
            'fec_nac' => ['required','date',new MayorEdadRule()],
            'ciudad' => 'required|integer|exists:ciudades,id',
        ],[
            'password.regex' => 'Su contraseña debe tener un número, una letra mayúscula, un carácter especial',
            'fec_nac.required' => 'Su fecha de nacimiento es requerida',
            'fec_nac.date' => 'Su fecha de nacimiento no tiene el formato correcto',
        ]);

        DB::beginTransaction();
        try{
            $usuario->nombre = $request->nombre;
            if(!empty($request->password)){ $usuario->password = bcrypt($request->password); }
            $usuario->celular = $request->celular;
            $usuario->fec_nac = $request->fec_nac;
            $usuario->ciudad_id = $request->ciudad;
            $usuario->save();
            DB::commit();
            return redirect('/usuarios');
        }catch (\Exception $ex){
            DB::rollBack();
            report($ex);
            return redirect()->back()->with(['errorInterno'=>'Ha ocurrido un error'])->withInput($request->all());
        }
    }

    public function delete(Request $request){
        $request->validate([
            'id' => 'required|numeric|min:1',
        ]);

        DB::beginTransaction();
        try{
            User::findOrFail($request->id)->delete();
            DB::commit();
            return response()->json(['status'=>200,'respuesta'=>'Usuario eliminado con éxito']);
        }catch (\Exception $ex){
            DB::rollBack();
            report($ex);
            return response()->json(['status'=>400,'respuesta'=>'Ha ocurrido un error']);
        }

    }

    public function perfil(){
        $usuario = auth()->user();
        $usuario->load(['ciudad:id,nombre,estado_id','ciudad.estado:id,nombre,pais_id','ciudad.estado.pais:id,nombre']);
        return view('usuarios.perfil',['usuario'=>$usuario,]);
    }
}
