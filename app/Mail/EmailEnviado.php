<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailEnviado extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $remitente;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email, $remitente)
    {
        $this->email = $email;
        $this->remitente = $remitente;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->remitente)->markdown('emails.enviado', ['mensaje' => $this->email->mensaje]);
    }
}
