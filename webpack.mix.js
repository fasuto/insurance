const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .version()
    .postCss('resources/css/app.css', 'public/css', [])
    .postCss('resources/css/login.css', 'public/css', [])
    .browserSync({
        open: 'external',
        host: 'insurance.test',
        proxy: 'insurance.test',
        files: ['app/**/*', 'resources/**/*', 'public/js/*.js', 'public/css/*.css']
    })
    .options({ processCssUrls: false })
    ;
