<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = array(
            array( 'id'=>1, 'nombre'=>'Colombia'),
            array( 'id'=>2, 'nombre'=>'Ecuador'),
            array( 'id'=>3, 'nombre'=>'Perú')
        );

        DB::table('paises')->insert($records);
    }
}
