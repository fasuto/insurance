<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Creación de usuario administrador
         */

        $records = array(
            array('nombre'=>'Fausto Cevallos','email'=>'fausto@email.com','cedula'=>'1212121212','celular'=>'0979076215','admin'=>true,'fec_nac'=>'1991/07/05','password'=>bcrypt('Admin123456$'),'ciudad_id'=>5),
            array('nombre'=>'Danilo Muñoz','email'=>'danilo@email.com','cedula'=>'1212121212','celular'=>'0979076215','admin'=>false,'fec_nac'=>'1991/07/05','password'=>bcrypt('User123456$'),'ciudad_id'=>5),
        );

        DB::table('users')->insert($records);
    }
}
