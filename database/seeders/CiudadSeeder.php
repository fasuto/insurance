<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = array(
            array( 'id'=>1, 'nombre'=>'Bogotá', 'estado_id' => 1),
            array( 'id'=>2, 'nombre'=>'Barrios Unidos', 'estado_id' => 1),
            array( 'id'=>3, 'nombre'=>'Cartagena', 'estado_id' => 2),
            array( 'id'=>4, 'nombre'=>'Cartagena', 'estado_id' => 2),
            array( 'id'=>5, 'nombre'=>'Quito', 'estado_id' => 3),
            array( 'id'=>6, 'nombre'=>'Machachi', 'estado_id' => 3),
            array( 'id'=>7, 'nombre'=>'Guayaquil', 'estado_id' => 4),
            array( 'id'=>8, 'nombre'=>'Durán', 'estado_id' => 4),
            array( 'id'=>9, 'nombre'=>'Cajamarca', 'estado_id' => 5),
            array( 'id'=>10, 'nombre'=>'Cusco', 'estado_id' => 6),
        );

        DB::table('ciudades')->insert($records);
    }
}
