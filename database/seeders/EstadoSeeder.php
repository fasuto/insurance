<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = array(
            array( 'id'=>1, 'nombre'=>'Bogotá', 'pais_id' => 1),
            array( 'id'=>2, 'nombre'=>'Bolívar', 'pais_id' => 1),
            array( 'id'=>3, 'nombre'=>'Pichincha', 'pais_id' => 2),
            array( 'id'=>4, 'nombre'=>'Guayas', 'pais_id' => 2),
            array( 'id'=>5, 'nombre'=>'Cajamarca', 'pais_id' => 3),
            array( 'id'=>6, 'nombre'=>'Cusco', 'pais_id' => 3)
        );

        DB::table('estados')->insert($records);
    }
}
