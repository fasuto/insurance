<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    /*
     *Verifica si el usuario está autenticado
     */
    return auth()->check() ? redirect('/home') : view('welcome');
});

Route::get('/login', function (){ return view('welcome'); });
Route::get('/emails/endpoint', ['App\Http\Controllers\EmailsController','endpoint']);

/*
 * Rutas de autenticación
 * */
Route::post('/login', ['App\Http\Controllers\LoginController','authenticate'])->name('login');
Route::post('/logout', ['App\Http\Controllers\LoginController','logout'])->name('logout');

/*
 * Rutas protegidas
 * */
Route::group(['middleware'=>['web','auth']],function(){
    Route::get('/home', function () { return view('home'); });
    Route::get('/perfil', ['App\Http\Controllers\UsuariosController','perfil']);

    Route::get('/emails', ['App\Http\Controllers\EmailsController','index']);
    Route::get('/emails/crear', ['App\Http\Controllers\EmailsController','crear']);
    Route::post('/emails/crear', ['App\Http\Controllers\EmailsController','almacenar']);
});

/*
 * Rutas de administración
 */
Route::group(['middleware'=>['web','auth','isAdmin']],function(){
    Route::get('/usuarios', ['App\Http\Controllers\UsuariosController','index']);
    Route::get('/usuarios/crear', ['App\Http\Controllers\UsuariosController','create']);
    Route::post('/usuarios/crear', ['App\Http\Controllers\UsuariosController','store']);
    Route::delete('/usuarios', ['App\Http\Controllers\UsuariosController','delete']);
    Route::get('/usuarios/{usuario}/editar', ['App\Http\Controllers\UsuariosController','editar']);
    Route::post('/usuarios/{usuario}/editar', ['App\Http\Controllers\UsuariosController','actualizar']);

    Route::post('/estados', ['App\Http\Controllers\ServiciosController','estados']);
    Route::post('/ciudades', ['App\Http\Controllers\ServiciosController','ciudades']);
});
