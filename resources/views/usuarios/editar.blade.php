@extends('layout.admin',['menu'=>'usuarios'])

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Editar usuario</h5>
        </div>
        <div class="card-body">
            <form action="{{ url('/usuarios/'.$usuario->id.'/editar')  }}" method="POST" autocomplete>
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" id="nombre" value="{{ $usuario->nombre }}" placeholder="Nombres y Apellidos" required>
                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="cedula">Cédula</label>
                        <input type="text" class="form-control  @error('cedula') is-invalid @enderror" name="cedula" id="cedula" value="{{ $usuario->cedula }}" placeholder="0123456789" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="fechaNacimiento">Fecha de Nacimiento</label>
                        <input type="date" class="form-control  @error('fec_nac') is-invalid @enderror" name="fec_nac" id="fechaNacimiento" value="{{ $usuario->fec_nac }}" required>
                        @error('fec_nac')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control  @error('celular') is-invalid @enderror" name="celular" id="celular" value="{{ $usuario->celular }}" placeholder="0979708585">
                        @error('celular')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="email">Email</label>
                        <input type="email" class="form-control  @error('email') is-invalid @enderror" id="email" name="email" value="{{ $usuario->email }}" placeholder="usuario@correo.com" disabled>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="password">Contraseña</label>
                        <input type="password" class="form-control  @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="password_confirmation">Confirmar contraseña</label>
                        <input type="password" class="form-control  @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password" placeholder="Re Password">
                        @error('password_confirm')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="pais">País</label>
                        <select id="pais" name="pais" class="form-control">
                            <option value="-1" selected>Seleccione</option>
                            @foreach( $paises as $pais )
                                <option value="{{ $pais->id }}" @if($pais->id == $usuario->ciudad->estado->pais_id) selected @endif>{{ $pais->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estado">Estado</label>
                        <select id="estado" name="estado" class="form-control">
                            <option value="{{$usuario->ciudad->estado->id}}" selected> {{ $usuario->ciudad->estado->nombre }}</option>
                            <option value="-1">Seleccione</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="ciudad">Ciudad</label>
                        <select id="ciudad" name="ciudad" class="form-control  @error('ciudad') is-invalid @enderror" required>
                            <option value="{{$usuario->ciudad->id}}" selected> {{ $usuario->ciudad->nombre }}</option>
                            <option value="-1">Seleccione</option>
                        </select>
                        @error('ciudad')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-12">
                        @if(!empty(Session::get('errorInterno')))
                            <span class="text-danger" role="alert">
                                <strong>{{ Session::get('errorInterno') }} - Por favor inténtelo más tarde</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Editar</button>
                <a class="btn btn-secondary" href="{{ url('/usuarios')  }}" role="button">Cancelar</a>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            /*
             * Realiza la consulta de estados por país
             */
            $('#pais').change(function () {
                let id = $(this).children("option:selected").val();

                if(id == -1){ return false; }

                clearSelection('#estado')
                clearSelection('#ciudad')

                $.ajax({
                    type: 'POST',
                    url: "{{ url('/estados') }}",
                    dataType: 'json',
                    data: {
                        'pais': id,
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        $('#estado').empty();
                        $('#estado').append('<option value=-1> Seleccione </option>');
                        for (var i = 0; i < data.length; i++) {
                            $('#estado').append('<option value=' + data[i].id + '> ' + data[i].nombre +'</option>');
                        }
                    }
                });
            });

            /*
             * Realiza la consulta de ciudades por estado
             */
            $('#estado').change(function () {
                let id = $(this).children("option:selected").val();

                if(id == -1){ return false; }

                clearSelection('#ciudad')

                $.ajax({
                    type: 'POST',
                    url: "{{ url('/ciudades') }}",
                    dataType: 'json',
                    data: {
                        'estado': id,
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        $('#ciudad').empty();
                        $('#ciudad').append('<option value=-1> Seleccione </option>');
                        for (var i = 0; i < data.length; i++) {
                            $('#ciudad').append('<option value=' + data[i].id + '> ' + data[i].nombre +'</option>');
                        }
                    }
                });

            });

            function clearSelection(id){
                $(id).empty();
                $(id).append('<option value=-1> Seleccione </option>');
            }
        });
    </script>
@endsection
