@extends('layout.admin',['menu'=>'perfil'])

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Perfil de usuario</h5>
        </div>
        <div class="card-body">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" value="{{ $usuario->nombre }}" placeholder="Nombres y Apellidos" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="cedula">Cédula</label>
                        <input type="text" class="form-control" name="cedula" id="cedula" value="{{ $usuario->cedula }}" placeholder="0123456789" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="fechaNacimiento">Fecha de Nacimiento</label>
                        <input type="date" class="form-control" name="fec_nac" id="fechaNacimiento" value="{{ $usuario->fec_nac }}" disabled>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control" name="celular" id="celular" value="{{ $usuario->celular }}" placeholder="0979708585" disabled>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ $usuario->email }}" placeholder="usuario@correo.com" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="pais">País</label>
                        <select id="pais" name="pais" class="form-control" disabled>
                            <option selected>{{ $usuario->ciudad->estado->pais->nombre }}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estado">Estado</label>
                        <select id="estado" name="estado" class="form-control" disabled>
                            <option selected>{{ $usuario->ciudad->estado->nombre }}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="ciudad">Ciudad</label>
                        <select id="ciudad" name="ciudad" class="form-control  @error('ciudad') is-invalid @enderror" disabled>
                            <option selected>{{ $usuario->ciudad->nombre }}</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
