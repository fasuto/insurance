@extends('layout.admin',['menu'=>'usuarios'])

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Administración de usuarios</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <a class="btn btn-success" href="{{ url('/usuarios/crear') }}" role="button">Agregar</a>
                </div>
                <div class="col">
                    <form class="form-inline mb-3 float-right" action="{{ url('usuarios') }}" method="GET">
                        <div class="form-group">
                            <label for="porPagina">Reg./Pág.</label>
                            <input type="number" id="porPagina" name="porPagina" class="form-control mx-sm-3" style="width: 4em" value="{{ $porPagina }}">
                        </div>
                        <div class="form-group">
                            <label for="filtro">Filtro</label>
                            <input type="text" id="filtro" class="form-control mx-sm-3" name="filtro" title="Filtro por: Nombre/Cédula/Celular/Email" value="{{ $filtro }}" placeholder="Filtro">
                        </div>

                        <button type="submit" class="btn btn-primary my-1">Buscar</button>
                    </form>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger pt-2 pb-2" style="text-align: left">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Cédula</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Celular</th>
                        <th scope="col">Email</th>
                        <th scope="col">F. Nacimiento</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Ciudad</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($usuarios as $user)
                        <tr scope="row">
                            <td>{{$user->id}}</td>
                            <td>{{$user->cedula}}</td>
                            <td>{{$user->nombre}}</td>
                            <td>{{$user->celular}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->fec_nac}}</td>
                            <td>{{$user->edad}}</td>
                            <td>{{$user->ciudad->nombre}}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ url('/usuarios/'.$user->id.'/editar') }}" role="button">Editar</a>
                                <a class="eliminar btn btn-dark" data-id="{{ $user->id }}" role="button">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col">
                    <div class="float-right">
                        {{ $usuarios->appends(request()->input())->onEachSide(2)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            /*
            * Elimina un usuario
            * */
            $(document).on('click','.eliminar',function (){

                if(!confirm("Desea eliminar el registro?")) { return false; }

                let element = $(this)

                $.ajax({
                    type: 'POST',
                    url: "{{ url('/usuarios') }}",
                    dataType: 'json',
                    data: {
                        'id': element.data("id"),
                        '_token': "{{ csrf_token() }}",
                        '_method': "delete"
                    },
                    success: function (data) {
                        alert(data.respuesta)
                        if(data.status == 200){
                            element.closest('tr').remove();
                        }
                    },
                    error:function (){
                        alert('Ha ocurrido un error, intentelo más tarde!')
                    }
                });
            })
        })
    </script>
@endsection
