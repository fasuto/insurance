@extends('layout.public')

@section('content')
@section('content')
    <div class="card mt-4">
        <div class="card-header">
            <h5 class="card-title">Bitácorea de Emails</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <form class="form-inline mb-3 float-right" action="{{ url('/emails/endpoint') }}" method="GET">
                        <div class="form-group">
                            <label for="porPagina">Reg./Pág.</label>
                            <input type="number" id="porPagina" name="porPagina" class="form-control mx-sm-3" style="width: 4em" value="{{ $porPagina }}">
                        </div>
                        <div class="form-group">
                            <label for="filtro">Filtro</label>
                            <input type="text" id="filtro" class="form-control mx-sm-3" name="filtro" title="Filtro por: Aunto/Destinatario/Remitente" value="{{ $filtro }}" placeholder="Filtro">
                        </div>
                        <button type="submit" class="btn btn-primary my-1">Buscar</button>
                    </form>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger pt-2 pb-2" style="text-align: left">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Asunto</th>
                    <th scope="col">Destinatario</th>
                    <th scope="col">Remitente</th>
                    <th scope="col">Estado</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($emails as $email)
                    <tr scope="row">
                        <td>{{$email->asunto }}</td>
                        <td>{{$email->destinatario }}</td>
                        <td>{{$email->usuario->email }}</td>
                        <td>{{$email->estado ? 'Enviado' : 'No enviado' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col">
                    <div class="float-right">
                        {{ $emails->appends(request()->input())->onEachSide(2)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@endsection
