@extends('layout.admin',['menu'=>'inicio'])

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Panel de Administración</h5>
        </div>
        <div class="card-body">
            <p class="card-text">Bienvenido al panel de administración del sistema web.</p>
        </div>
    </div>
@endsection
