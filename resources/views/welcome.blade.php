@extends('layout.public')

@section('content')
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <div class="fadeIn first pt-4 pb-4">
                <a class="underlineHover" href="#">Inicio de Sesión</a>
            </div>

            <form method="POST" action="{{ url('/login') }}" autocomplete="off">
                @csrf
                <input type="email" id="login" class="fadeIn second" value="{{ old('email') }}" name="email" placeholder="Email" required>
                <input type="password" id="password" class="fadeIn third" name="password"  placeholder="Password" required>
                <input type="submit" class="fadeIn fourth" value="Acceder">
            </form>

            @if ($errors->any())
                <div class="alert alert-danger" style="text-align: left">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif

            <div id="formFooter">
                <a class="underlineHover" href="#">Forgot Password?</a>
            </div>
        </div>
    </div>
@endsection
