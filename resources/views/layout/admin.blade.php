<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administración</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    @yield('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/home') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto"> </ul>

                <ul class="navbar-nav ml-auto">
                    <li>
                        <a class="nav-link" href="{{ url('/usuarios/perfil') }}" role="button">
                            {{ Auth::user()->nombre }}
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="#" role="button" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="container-fluid mt-2">
        <div class="row">
            <div class="col-2">
                <div class="list-group p-0 m-0">
                    <span class="list-group-item list-group-item-dark"> Menú </span>
                    <a href="{{ url('/home')  }}" class="list-group-item list-group-item-action @if($menu == 'inicio') active @endif ">Inicio</a>
                    <a href="{{ url('/perfil')  }}" class="list-group-item list-group-item-action @if($menu == 'perfil') active @endif ">Perfil</a>
                    @if(auth()->user()->isAdmin())
                        <a href="{{ url('/usuarios')  }}" class="list-group-item list-group-item-action @if($menu == 'usuarios') active @endif ">Usuarios</a>
                    @endif
                    <a href="{{ url('/emails')  }}" class="list-group-item list-group-item-action @if($menu == 'emails') active @endif ">Emails</a>
                </div>
            </div>
            <div class="col-10">
                @yield('content')
            </div>
        </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    @yield('scripts')
</body>
</html>
