@extends('layout.admin',['menu'=>'usuarios'])

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Enviar email</h5>
        </div>
        <div class="card-body">
            <form action="{{ url('/emails/crear')  }}" method="POST" autocomplete>
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="asunto">Asunto</label>
                        <input type="text" class="form-control @error('asunto') is-invalid @enderror" name="asunto" id="asunto" value="{{ old('asunto') }}" placeholder="Asunto" required>
                        @error('asunto')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-8">
                        <label for="destinatario">Destinatario</label>
                        <input type="email" class="form-control  @error('destinatario') is-invalid @enderror" name="destinatario" id="destinatario" value="{{ old('destinatario') }}" placeholder="dest@email.com" required>
                        @error('destinatario')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="form-group">
                            <label for="mensaje">Mensaje</label>
                            <textarea class="form-control @error('mensaje') is-invalid @enderror" id="mensaje" rows="3" name="mensaje">{{ old('mensaje') }}</textarea>
                        </div>
                        @error('mensaje')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        @if(!empty(Session::get('errorInterno')))
                            <span class="text-danger" role="alert">
                                <strong>{{ Session::get('errorInterno') }} - Por favor inténtelo más tarde</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
                <a class="btn btn-secondary" href="{{ url('/emails')  }}" role="button">Cancelar</a>
            </form>
        </div>
    </div>
@endsection
