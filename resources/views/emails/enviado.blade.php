@component('mail::message')

Estimado/a,

{{ $mensaje }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
